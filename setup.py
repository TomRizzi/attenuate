from setuptools import setup

VERSION = '0.0.1'


with open("README.md", "rb") as f:
    long_descr = f.read().decode("utf-8")

setup(
    name="attenuator",
    packages=["attenuator"],
    entry_points={
        "console_scripts": ['attn = attenuator.__main__:run']
        },
    version=VERSION,
    long_description=long_descr,
    author="Tom Rizzi",
    author_email="tom.rizzi@mimomax.co.nz",
    install_requires=['gpiozero'],
    include_package_data=True
    )
