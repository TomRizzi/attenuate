# Attenuator Script
Python cli script to control a Mini-Circuits digital step attenuator through a raspberry pis GPIO.

## Installing
1. Clone the source code
    ```bash
    git clone https://gitlab.com/TomRizzi/attenuate
    ```
2. Change directory into the cloned repository
    ```bash
    cd attneuate
    ```
3. Install using pip
    ```bash
    pip3 install -e .
    ```

## Setup
Each attenuator attached to the pi's GPIO needs to be defined in [pin_conf.py](attenuator/pin_conf.py) as a 
python dictionary and added to the `ATTENS` array.
Each wire needs to be mapped to a GPIO port based on the wires colour.
```python
PINS_1 = {
    "white": 11,
    "red": 19,
    "brown": 26,
    "orange": 13,
    "blue": 0,
    "green": 5,
    "yellow": 6
}

ATTENS = [PINS_1]
```
Each pin number references a GPIO pin. 
For the example above, the "white" wire above would be connected to the pin labeled "GPIO11" in green in the image below. 

![](images/gpio.png)

## Usage
```bash
usage: attn [-h] {run-range,set} ...

Change the attenuation of a mini circuits attenuator

positional arguments:
  {run-range,set}
    run-range      Step through attenuation range
    set            Set attenuation for one attenuator

optional arguments:
  -h, --help       show this help message and exit
```
### Setting the attenuation
The set sub command will set the attenuation level of an attenuator. 
```bash
attn set {attenuator_id} {db_level}
```
Where... 
- `attenuator_id` is the index of the attenuator to set the level on as defined in the `ATTENS` list in [pin_conf.py](attenuator/pin_conf.py)
- `db_level` is the level of attenuation to set it to

For example, the following would set the db level on the first attenuator to 14.5db. 
```bash
attn set 0 14.5
```

### Iterating through a range of attenuation levels
The run-range sub-command will iterate from 0db to 31.5db then back to 0db in 5 second intervals on the specified attenuator.
```bash
attn run-range {attenuator_id}
```

Where... 
- `attenuator_id` is the index of the attenuator to set the level on as defined in the `ATTENS` list in [pin_conf.py](attenuator/pin_conf.py)
