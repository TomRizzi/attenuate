"""
MINI CIRCUITS CABLE
------------------------------------------------------
LE Latch Enable Input                       WHITE
C1 Control for attenuation bit, 1 dB        YELLOW
C0.5 Control for attenuation bit, 0.5 dB    GREEN
C16 Control for attenuation bit, 16 dB      BLUE
GND Ground connection                       BLACK
C4 Control for attenuation bit, 4 dB        ORANGE
C8 Control for attenuation bit, 8 dB        BROWN
C2 Control for attenuation bit, 2 dB        RED
"""

"""
Each pin dict should be a mapping from wire colour -> GPIO Pin
Where each wire colour is the colour of the wire in the Mini Circuits cable in the attenuator
"""
PINS_1 = {
    "white": 11,
    "red": 19,
    "brown": 26,
    "orange": 13,
    "blue": 0,
    "green": 5,
    "yellow": 6
}

ATTENS = [PINS_1]
