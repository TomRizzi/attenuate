from gpiozero import LED
from time import sleep
from .pin_conf import ATTENS


class Attenuator:

    DB_VALUES = (0.5, 1, 2, 4, 8, 16)

    def __init__(self, gpio_pins: tuple):
        """
        :param gpio_pins: 7 tuple of gpio pins (latch_enable, c0.5, ..., c16)
        """
        self.latch_pin = gpio_pins[0]
        self.pins = gpio_pins[1:]

    def _enable_latch(self):
        """ Enables the latch on the attenuator """
        self.latch_pin.on()

    def _disable_latch(self):
        """ Disables the latch on the attenuator """
        self.latch_pin.off()

    def _set_pins(self, values):
        """
        Sets the gpio pins
        :param values: 6-tuple of bools corresponding to the state for each pin
        :return: None
        """
        try:
            self._enable_latch()
            if len(values) != 6:
                raise Exception('Incorrect values length given')

            for pin, turn_on in zip(self.pins, values):
                if turn_on:
                    pin.on()
                else:
                    pin.off()
        finally:
            self._disable_latch()

    def set_attenuation(self, db_level):
        """
        Multiplexes the db_level to the closest binary value
        Sets the attenuation level
        :param db_level: DB level of attenuation
        :return: None
        """
        db_remaining = db_level
        bin_values = ()

        # Iterate through the possible db_values and get as close to the specified level as possible
        for level in reversed(self.DB_VALUES):
            if level <= db_remaining:
                bin_values = (True,) + bin_values
                db_remaining -= level
            else:
                bin_values = (False,) + bin_values

        actual_level = db_level - db_remaining
        print(f'Setting attenuation to {actual_level}db')

        # Set the corresponding pins
        self._set_pins(bin_values)

    def step_up_and_down(self):
        bottom = 0
        top = 32

        for val in range(bottom, top):
            self.set_attenuation(val)
            sleep(5)

        for val in range(top - 1, bottom, -1):
            self.set_attenuation(val)
            sleep(5)


def attenuator_builder(pin_mappings):
    """ Takes the pin number that each colour wire is connected to
        and creates an Attenuator object
    """
    pin_order = ["white", "green", "yellow", "red", "orange", "brown", "blue"]
    if any([pin not in pin_mappings for pin in pin_order]):
        raise Exception('Not all pin mappings provided')

    pins = (LED(pin_mappings[pin]) for pin in pin_order)
    return Attenuator(tuple(pins))






if __name__ == '__main__':
    atten = attenuator_builder(ATTENS[0])
    atten.set_attenuation(14)

