from .set_attenuation import attenuator_builder
from .pin_conf import ATTENS
import argparse


def run():
    parser = argparse.ArgumentParser(
        description='Change the attenuation of a mini circuits attenuator')
    sp = parser.add_subparsers(dest='sub-command', required=True)

    # Create 'run-range' sub parser
    sp_run = sp.add_parser('run-range', help='Step through attenuation range',
                           description='Step through attenuation range')
    sp_run.add_argument('attenuators', type=int, nargs='*', help='Attenuators to run the range on')
    sp_run.set_defaults(func=step_through)

    # Set level
    sp_set = sp.add_parser('set', help='Set attenuation for one attenuator',
                           description='Set attenuation for one attenuator')
    sp_set.add_argument('attenuator_id', type=int)
    sp_set.add_argument('level', type=float)
    sp_set.set_defaults(func=set_attn)

    args = parser.parse_args()
    args.func(args)


def step_through(args):
    attenuators = args.attenuators or [0]
    atten = attenuator_builder(ATTENS[attenuators[0]])
    atten.step_up_and_down()


def set_attn(args):
    attn_id = args.attenuator_id
    level = args.level
    atten = attenuator_builder(ATTENS[attn_id])
    atten.set_attenuation(level)


if __name__ == '__main__':
    run()

